package com.pcxhin.lemans.rest.controller;

import com.pcxhin.lemans.servicelayer.model.User;
import com.pcxhin.lemans.servicelayer.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/user")
    public List<User> get() { return userService.get(); }

    @PostMapping("/user")
    public User save(@RequestBody User userObj){
        userService.save(userObj);
        return userObj;
    }

    @GetMapping("/user/{id}")
    public User get(@PathVariable int id){
        User userObj = userService.get(id);
        if(userObj == null){
            throw new RuntimeException("User with id " + id + "was not found!");
        }
        return userObj;
    }

    @DeleteMapping("user/{id}")
    public String delete(@PathVariable int id){
        userService.delete(id);
        return "User has ben deleted with id:" + id;
    }

    @PutMapping("/user")
    public User update(@RequestBody User userObj){
        userService.save(userObj);
        return userObj;
    }
}
