package com.pcxhin.lemans.rest.controller;

import com.pcxhin.lemans.servicelayer.dto.ApplicationDTO;
import com.pcxhin.lemans.servicelayer.model.Application;
import com.pcxhin.lemans.servicelayer.model.Category;
import com.pcxhin.lemans.servicelayer.service.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ApplicationController {

    @Autowired
    private ApplicationService applicationService;

    @GetMapping("/application")
    public List<ApplicationDTO> get() {
        return applicationService.getAllApplications();
    }

    @PostMapping("/application")
    public void save(@RequestBody ApplicationDTO applicationObj){
        applicationService.save(applicationObj);
    }

    @GetMapping("/application/{id}")
    public Application get(@PathVariable int id){
        Application applicationObj = applicationService.get(id);
        if(applicationObj == null){
            throw new RuntimeException("Application with id " + id + "was not found!");
        }
        return applicationObj;
    }

    @DeleteMapping("application/{id}")
    public String delete(@PathVariable int id){
        applicationService.delete(id);
        return "Application has ben deleted with id:" + id;
    }

    @PutMapping("/application")
    public void update(@RequestBody ApplicationDTO applicationObj){
        applicationService.save(applicationObj);
    }

    @GetMapping("/categories")
    public List<Category> getCat() { return applicationService.getCat(); }
}
