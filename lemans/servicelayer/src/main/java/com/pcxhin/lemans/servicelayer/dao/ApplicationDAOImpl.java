package com.pcxhin.lemans.servicelayer.dao;

import com.pcxhin.lemans.servicelayer.model.Application;
import com.pcxhin.lemans.servicelayer.model.Category;
import com.pcxhin.lemans.servicelayer.model.Status;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class ApplicationDAOImpl implements ApplicationDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Application> getAllApplications() {
        TypedQuery<Application> query = entityManager.createQuery("Select application from Application application", Application.class);
        return query.getResultList();
    }

    @Override
    public Application getAllApplications(int id) {
        Session currentSession = entityManager.unwrap(Session.class);
        Application applicationObj = currentSession.get(Application.class, id);
        return applicationObj;
    }

    @Override
    public void save(Application application) {
        entityManager.merge(application);
    }

    @Override
    public void delete(int id) {
        Session currentSession = entityManager.unwrap(Session.class);
        Application applicationObj = currentSession.get(Application.class, id);
        currentSession.delete(applicationObj);
    }

    @Override
    public List<Category> getCat() {
        Session currentSession = entityManager.unwrap(Session.class);
        Query<Category> query = currentSession.createQuery("from Category", Category.class);
        List<Category> list = query.getResultList();
        return list;
    }

    @Override
    public Category findCategoryById(int id) {
        TypedQuery<Category> query = entityManager.createQuery("Select category From Category category Where category.id = :id", Category.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    @Override
    public Status findStatusById(int id) {
        TypedQuery<Status> query = entityManager.createQuery("Select s From Status s Where s.id = :id", Status.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }
}
