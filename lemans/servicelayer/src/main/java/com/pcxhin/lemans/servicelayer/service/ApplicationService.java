package com.pcxhin.lemans.servicelayer.service;

import com.pcxhin.lemans.servicelayer.dto.ApplicationDTO;
import com.pcxhin.lemans.servicelayer.model.Application;
import com.pcxhin.lemans.servicelayer.model.Category;

import java.util.List;

public interface ApplicationService {

        List<ApplicationDTO> getAllApplications();

        Application get(int id);

        void save(ApplicationDTO application);

        void delete(int id);

        List<Category> getCat();
}
