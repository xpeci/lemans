package com.pcxhin.lemans.servicelayer.service;

import com.pcxhin.lemans.servicelayer.dao.ApplicationDAO;
import com.pcxhin.lemans.servicelayer.dao.UserDAO;
import com.pcxhin.lemans.servicelayer.dto.ApplicationDTO;
import com.pcxhin.lemans.servicelayer.model.Application;
import com.pcxhin.lemans.servicelayer.model.Category;
import com.pcxhin.lemans.servicelayer.model.Status;
import com.pcxhin.lemans.servicelayer.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ApplicationServiceImpl implements ApplicationService {

    @Autowired
    private ApplicationDAO applicationDAO;

    @Autowired
    UserDAO userDAO;

    @Transactional
    @Override
    public List<ApplicationDTO> getAllApplications() {
        return applicationDAO.getAllApplications()
                .stream()
                .map(toApplicationToDTO())
                .collect(Collectors.toList());
    }

    private Function<Application, ApplicationDTO> toApplicationToDTO() {
        return entity -> {
            ApplicationDTO dto = new ApplicationDTO();
            dto.setCategory(entity.getCategory().getId());
            dto.setComment(entity.getComment());
            dto.setStartDate(entity.getStart_date().toString());
            dto.setEndDate(entity.getEnd_date().toString());
            dto.setUserId(entity.getUser().getId());
            dto.setSupervisorId(entity.getSupervisor().getId());
            dto.setStatus(entity.getStatus().getStatus());
            dto.setColor(entity.getStatus().getColor());
            return dto;
        };
    }

    @Transactional
    @Override
    public Application get(int id) {
        return applicationDAO.getAllApplications(id);
    }

    @Transactional
    @Override
    public void save(ApplicationDTO application) {
        Application entity = new Application();

        entity.setComment(application.getComment());

        User user = userDAO.get(application.getUserId());
        User supervisor = userDAO.get(application.getSupervisorId());
        Category category = applicationDAO.findCategoryById(application.getCategory());
        Status status = applicationDAO.findStatusById(application.getStatusId());
        //TODO: add more fields

        // user.getApplications().add(entity);

        entity.setUser(user);
        entity.setSupervisor(supervisor);
        entity.setCategory(category);
        entity.setStatus(status);

        applicationDAO.save(entity);
    }

    @Transactional
    @Override
    public void delete(int id) {
        applicationDAO.delete(id);
    }

    @Transactional
    @Override
    public List<Category> getCat() {
        return applicationDAO.getCat();
    }

}
