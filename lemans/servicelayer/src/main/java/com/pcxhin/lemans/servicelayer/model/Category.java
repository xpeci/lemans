package com.pcxhin.lemans.servicelayer.model;

import javax.persistence.*;

@Entity
@Table(name = "leman_category")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;

    @Column
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String status) {
        this.name = name;
    }

}
