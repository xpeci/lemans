package com.pcxhin.lemans.servicelayer.dao;

import com.pcxhin.lemans.servicelayer.model.Application;
import com.pcxhin.lemans.servicelayer.model.Category;
import com.pcxhin.lemans.servicelayer.model.Status;

import java.util.List;

public interface ApplicationDAO {

    List<Application> getAllApplications();

    Application getAllApplications(int id);

    void save(Application application);

    void delete(int id);

    List<Category> getCat();

    Category findCategoryById(int id);

    Status findStatusById(int id);
}
