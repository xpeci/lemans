package com.pcxhin.lemans.servicelayer.service;

import com.pcxhin.lemans.servicelayer.model.User;
import java.util.List;

public interface UserService {
    List<User> get();

    User get(int id);

    void save(User user);

    void delete(int id);
}
