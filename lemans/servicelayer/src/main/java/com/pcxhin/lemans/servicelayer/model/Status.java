package com.pcxhin.lemans.servicelayer.model;

import javax.persistence.*;

@Entity
@Table(name = "leman_status")
public class Status {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;

    @Column
    private String status;

    @Column
    private String color;

    /*@OneToOne(mappedBy = "status")
    private Application application;*/

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
