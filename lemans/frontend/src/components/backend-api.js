import axios from 'axios'

const AXIOS = axios.create({
  baseURL: `/api`,
  timeout: 1000
});


export default {
    getCategories() {
        return AXIOS.get(`/categories`);
    },
    getLeaveApplications() {
        return AXIOS.get(`/application`);
    },
    getUser(userId) {
        return AXIOS.get(`/user/` + userId);
    },
    getUsers() {
        return AXIOS.get(`/user`);
    },
    deleteUser(userId) {
        return AXIOS.delete(`/user/` + userId);
    },
    createUser(user) {
        return AXIOS.post(`/user`, user);
    },
    getSecured(user, password) {
        return AXIOS.get(`/secured/`,{
            auth: {
                username: user,
                password: password
            }});
    }
}


