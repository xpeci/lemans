import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import store from './store'

//Fontawesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserEdit, faTrashAlt, faBan, faUnlockAlt} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(faBan, faUnlockAlt, faTrashAlt, faUserEdit)
Vue.component('font-awesome-icon', FontAwesomeIcon)

// VCalendar
import VCalendar from 'v-calendar';
Vue.use(VCalendar, {
    componentPrefix: 'vc'
});

Vue.config.productionTip = false;

// Bootstrap
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');

